package com.hpe.qa.tester.perfdb.model;

import com.github.javafaker.*;
import com.github.javafaker.Number;

import java.sql.*;
import java.util.Date;
import java.util.UUID;

public class dbFaker {

    public static void main(String[] argv) {

        System.out.println("-------- PostgreSQL " + "JDBC Connection Testing ------------");

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your PostgreSQL JDBC Driver? " + "Include in your library path!");
            e.printStackTrace();
            return;
        }

        System.out.println("PostgreSQL JDBC Driver Registered!");
        Connection connection = null;

        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/perf_db", "perf_dbadmin", "123");
            Faker faker = new Faker();
            Number n = faker.number();
            String sqlST = "INSERT INTO perf.perfdatadetails(detailsid,machinename,category,perfdataname) VALUES(?,?,?,?);";
            int loop = 100;
            Company cp = faker.company();
            while(loop-->0){
                PreparedStatement ps = connection.prepareStatement(sqlST);
                ps.setInt(1,n.numberBetween(10000,99999));
                ps.setString(2,cp.name());
                ps.setString(3,cp.industry());
                ps.setObject(4,cp.buzzword());
                ps.execute();
            }
            connection.close();

            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/perf_db", "perf_dbadmin", "123");
            faker = new Faker();
            sqlST = "INSERT INTO perf.testrun(runid, displaystring,logstarttime,logstoptime," +
                    "numberofrecords,minutestotc,timezonename,currentrecordindex) " +
                    "VALUES(?,?,'20100301','20100302',?,?,?,?);";
            loop = 100;
            while(loop-->0){
                PreparedStatement ps = connection.prepareStatement(sqlST);
                UUID id = new UUID(1,loop);
                ps.setObject(1,id);
                ps.setObject(2,cp.name());
                ps.setInt(3,n.numberBetween(0,10));
                ps.setInt(4,n.numberBetween(1,1000));
                ps.setString(5,cp.name());
                ps.setInt(6,n.numberBetween(1,3));
                ps.execute();
            }
            connection.close();

            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/perf_db", "perf_dbadmin", "123");
            faker = new Faker();
            sqlST = "INSERT INTO perf.perfdata(runid,detailsid,recordindex,pdtimestamp,pdvalue,multicount,log) " +
                    "VALUES(?,?,?,?,?,?,?);";
            loop = 100;
            while(loop-->0){
                PreparedStatement ps = connection.prepareStatement(sqlST);

                UUID id = new UUID(1,loop);
                ps.setObject(1,id);
                ps.setObject(2,id);
                ps.setInt(3,n.numberBetween(1,10));
                ps.setTimestamp(4, Timestamp.valueOf("2016/01/01 12:12:12"));
                ps.setInt(5,n.numberBetween(1,10000));
                ps.setInt(6,n.numberBetween(1,5));
                ps.setString(7, cp.buzzword());
                ps.execute();
            }
            connection.close();


        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("You made it, take control your database now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }
}