package com.hpe.qa.tester.perfdb.model;

import java.util.Date;
import java.util.UUID;


public class perfData {



        private UUID runID;
        private int detailsID;
        private int  recordIndex;
        private Date pdTimeStamp;
        private float pdValue;
        private int multiCount;
        private String log;


        public perfData(UUID runID, int detailsID, int recordIndex,Date pdTimeStamp,float pdValue,int multiCount,String log){
            super();
            this.runID = runID;
            this.detailsID = detailsID;
            this.recordIndex = recordIndex;
            this.pdTimeStamp = pdTimeStamp;
            this.pdValue = pdValue;
            this.multiCount = multiCount;
            this.log = log;

        }

        public UUID getRunID() {
            return runID;
        }

        public int getDetailsID() {
            return detailsID;
        }

        public int getRecordIndex() {
            return recordIndex;
        }

        public Date getPdTimeStamp() {
            return pdTimeStamp;
        }

        public float getPdValue() {
            return pdValue;
        }

        public int getMultiCount() {
            return multiCount;
        }

        public String getLog() {
            return log;
        }



}
