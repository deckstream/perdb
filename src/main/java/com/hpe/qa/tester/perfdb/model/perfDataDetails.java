package com.hpe.qa.tester.perfdb.model;


public class perfDataDetails {
    private Long detialsID;
    private  String machineName;
    private String category;
    private String perfDataName;


    public perfDataDetails(Long detialsID, String machineName, String category, String perfDataName){
        this.detialsID = detialsID;
        this.machineName = machineName;
        this.category = category;
        this.perfDataName = perfDataName;

    }

    public Long getDetialsID() {
        return detialsID;
    }

    public String getMachineName() {
        return machineName;
    }

    public String getCategory() {
        return category;
    }

    public String getPerfDataName() {
        return perfDataName;
    }
}

