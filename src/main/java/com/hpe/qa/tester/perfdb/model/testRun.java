package com.hpe.qa.tester.perfdb.model;

import java.util.Date;
import java.util.UUID;


public class testRun {
    private UUID runID;
    private String displayString;
    private Date logStartTime;
    private Date logStopTime;
    private int numberOfRecords;
    private int minutesToUTC;
    private String timeZoneName;
    private int currentRecordIndex;

    public testRun(UUID runID, String displayString, Date logStartTime, Date logStopTime, int numberOfRecords, int minutesToUTC, String timeZoneName, int currentRecordIndex) {
        super();

        this.runID = runID;
        this.displayString = displayString;
        this.logStartTime = logStartTime;
        this.logStopTime = logStopTime;
        this.numberOfRecords = numberOfRecords;
        this.minutesToUTC = minutesToUTC;
        this.timeZoneName = timeZoneName;
        this.currentRecordIndex = currentRecordIndex;
    }




    public UUID getRunID() {
        return runID;
    }

    public String getDisplayString() {
        return displayString;
    }

    public Date getLogStartTime() {
        return logStartTime;
    }

    public Date getLogStopTime() {
        return logStopTime;
    }

    public int getNumberOfRecords() {
        return numberOfRecords;
    }

    public int getMinutesToUTC() {
        return minutesToUTC;
    }

    public String getTimeZoneName() {
        return timeZoneName;
    }

    public int getCurrentRecordIndex() {
        return currentRecordIndex;
    }
}
