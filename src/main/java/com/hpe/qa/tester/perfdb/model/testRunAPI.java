package com.hpe.qa.tester.perfdb.model;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("/testrun")
public class testRunAPI {
    @GET
    @Produces("text/plain")
    public String defaultReverser() {
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();

        }

        System.out.println("PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {

            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/perf_db", "perf_dbadmin",
                    "123456");
            String s = "select * from perf.testrun";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(s);
            List<String> result = new ArrayList<String >();
            JSONArray mJSONArray = new JSONArray();
            int i =0;
            while(rs.next()) {
                JSONObject json = new JSONObject();
                json.put("DisplayString",String.valueOf(rs.getString(2)));
                json.put("start",String.valueOf(rs.getDate(3)));
                json.put("end",String.valueOf(rs.getDate(4)));
                StringBuilder temp = new StringBuilder();
                mJSONArray.put(i++, json);

            }
            return mJSONArray.toString();
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();

        }

        return "error";
    }

}