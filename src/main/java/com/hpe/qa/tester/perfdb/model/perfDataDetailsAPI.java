package com.hpe.qa.tester.perfdb.model;

import org.json.JSONArray;
import org.json.JSONObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("/perfdatadetails")
public class perfDataDetailsAPI {
    @GET
    @Produces("application/json")
    public String defaultReverser() {
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();

        }

        System.out.println("PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/perf_db", "perf_dbadmin",
                    "123456");
            String s = "select * from perf.perfdatadetails";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(s);
            List<String> result = new ArrayList<String >();
            JSONArray mJSONArray = new JSONArray(result);
            int i =0;
            while(rs.next()) {
                JSONObject json = new JSONObject();
                json.put("detailsid",String.valueOf(rs.getString(1)));
                json.put("machinename",rs.getString(2));
                json.put("category",rs.getString(3));
                json.put("perfdataname",rs.getString(4));
                mJSONArray.put(i++,json);

            }
            return mJSONArray.toString();
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();

        }
        return "error";
    }

}