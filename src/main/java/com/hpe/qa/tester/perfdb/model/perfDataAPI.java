package com.hpe.qa.tester.perfdb.model;

import org.json.JSONArray;
import org.json.JSONObject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Path("/perfdata")
public class perfDataAPI {
    @GET
    @Produces("application/json")
    public String defaultReverser() {
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();

        }

        System.out.println("PostgreSQL JDBC Driver Registered!");

        Connection connection = null;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/perf_db", "perf_dbadmin",
                    "123456");
            String s = "select * from perf.perfdata";
            Statement st = connection.createStatement();
            ResultSet rs = st.executeQuery(s);
            List<String> result = new ArrayList<String >();
            JSONArray mJSONArray = new JSONArray(result);
            int i =0;
            while(rs.next()) {
                JSONObject json = new JSONObject();
                json.put("recordindex",String.valueOf(rs.getString(3)));
                json.put("pdtimestamp",rs.getString(4));
                json.put("multicount",rs.getString(6));
                json.put("log",rs.getString(7));
                mJSONArray.put(i++,json);

            }
            return mJSONArray.toString();
        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();

        }
        return "error";
    }

}